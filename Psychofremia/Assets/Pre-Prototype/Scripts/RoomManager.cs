﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour {

    static public RoomManager instance;
    public bool doorsClosed = true;
    public bool changeRoom = false;
    public bool canRotate = false;
    public bool puzzleSolved = false;

	// Use this for initialization
	void Awake () {
		if(!instance)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
