﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateMonsterOnTrigger : MonoBehaviour {

    [SerializeField] private EnemyBehaviour e;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            e.ActivateMonster(true);
        }
    }
}
