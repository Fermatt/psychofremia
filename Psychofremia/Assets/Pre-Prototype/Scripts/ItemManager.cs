﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour {

    static public ItemManager instance;
    public GameObject item;

	// Use this for initialization
	void Awake () {
		if(!instance)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
	}
}
