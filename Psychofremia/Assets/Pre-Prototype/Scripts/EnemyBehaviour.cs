﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {
    [SerializeField] private SanityComp s;
    [SerializeField] private float maxTimer;
    private float timer;
    private float start;
    [SerializeField] private float timeStart;
    private bool isVisible = false;
    private bool cuidado = false;
    public bool monsterActive = false;
    public bool monsterMovement = false;
    public bool monsterStart = false;
    public bool monsterYoqSe = false;
    public Transform newPos;
    public Transform startPos;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 screenPoint = Camera.main.WorldToViewportPoint(gameObject.transform.position);
        isVisible = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
        if (monsterActive)
        {
            start += Time.deltaTime;
            if (start > timeStart)
            {
                monsterStart = true;

            }
            if (monsterStart)
            {
                timer += Time.deltaTime;
                
                if (timer > maxTimer)
                {
                    timer = 0;
                    if (isVisible)
                    {
                        s.downSanity(5);
                    }
                    else
                    {
                        s.downSanity(2);
                    }
                }
                if (monsterMovement)
                {
                    transform.position = newPos.position;
                }
            }
        }
	}

    public void ActivateMonster(bool move)
    {
        if (!monsterActive && monsterYoqSe)
        {
            monsterActive = true;
            monsterMovement = move;
            AkSoundEngine.PostEvent("Cuidado", gameObject);
            monsterYoqSe = false;
        }
    }

    public void UnactivateMonster(Transform t)
    {
        if (monsterActive)
        {
            monsterActive = false;
            transform.position = t.position;
            transform.rotation = t.rotation;
            monsterMovement = false;
            monsterStart = false;
            cuidado = false;
        }
    }

    public void changePos(Transform t)
    {
        if (monsterMovement)
            newPos = t;
        else
        {
            transform.position = t.position;
            transform.rotation = t.rotation;
            newPos = t;
            monsterYoqSe = true;
        }
    }
}
