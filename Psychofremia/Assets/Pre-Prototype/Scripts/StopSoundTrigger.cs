﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopSoundTrigger : MonoBehaviour {

    private void OnTriggerStay(Collider other)
    {
        if(Input.GetButtonDown("Fire1") && other.tag == "Player")
        {
            AkSoundEngine.StopAll(gameObject);
        }
    }
}
