﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPlace : MonoBehaviour {

    [SerializeField] private GameObject[] items;
    [SerializeField] private Transform[] points;
    private bool[] puzzle;

	// Use this for initialization
	void Start () {
        puzzle = new bool[items.Length];
        for(int i = 0; i < puzzle.Length; i++)
        {
            puzzle[i] = false;
        }
	}

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            if(ItemManager.instance.item && Input.GetButtonDown("Fire1"))
            {
                GameObject g = ItemManager.instance.item;
                for (int i = 0; i < items.Length; i++)
                {
                    if(g == items[i])
                    {
                        RoomManager.instance.canRotate = true;
                        g.GetComponent<ItemGrab>().objPlayer = points[i];
                        ItemManager.instance.item = null;
                        puzzle[i] = true;
                        int k = 0;
                        for(int j = 0; j < puzzle.Length; j++)
                        {
                            if(!puzzle[j])
                            {
                                k++;
                            }
                        }
                        if(k == 0)
                        {
                            RoomManager.instance.puzzleSolved = true;
                            AkSoundEngine.PostEvent("Bien_Hecho", gameObject);
                        }
                        else
                        {
                            AkSoundEngine.PostEvent("Vamos_Uno_Mas", gameObject);
                        }
                        i = items.Length;
                    }
                }
            }
        }
    }
}
