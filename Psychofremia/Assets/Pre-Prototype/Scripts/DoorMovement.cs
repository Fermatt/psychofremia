﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorMovement : MonoBehaviour {

    [SerializeField] private int velocity;
    [SerializeField] private int tiempoAbierto;
    [SerializeField] private Transform player;
    [SerializeField] private GameObject door;
    [SerializeField] private bool lockedDoor;

    private bool moveDoor = false;
    private bool closeDoor = false;
    private bool opened = false;
    private float rotateCounter = 0;
    private float timer = 0;
    private int dir = 0;


    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (!lockedDoor)
        {
            if (moveDoor || closeDoor)
            {
                RoomManager.instance.doorsClosed = false;
                float num = velocity * Time.deltaTime;
                rotateCounter += num;
                if (opened)
                {
                    door.transform.Rotate(0, 0, -dir * num);
                    if (rotateCounter > 90)
                    {
                        door.transform.rotation.SetEulerAngles(0, 0, 0);
                        opened = false;
                        moveDoor = false;
                        closeDoor = false;
                        rotateCounter = 0;
                        RoomManager.instance.doorsClosed = true;
                        AkSoundEngine.PostEvent("Cerrar_Puerta", gameObject);
                    }
                }
                else
                {
                    door.transform.Rotate(0, 0, dir * num);
                    if (rotateCounter > 90)
                    {
                        door.transform.rotation.SetEulerAngles(0, 0, dir * 90);
                        opened = true;
                        moveDoor = false;
                        rotateCounter = 0;
                        
                    }
                }
            }
        }
        else
        {
            if(RoomManager.instance.puzzleSolved)
            {
                lockedDoor = false;
            }
        }
        /*
        else if (opened)
        {
            timer += Time.deltaTime;
            if (timer > tiempoAbierto)
            {
                moveDoor = true;
                timer = 0;
            }
        }*/
	}

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            if(Input.GetButtonDown("Fire1") && !opened && !moveDoor)
            {
                if (!lockedDoor)
                {
                    moveDoor = true;
                    Vector3 relativePoint = transform.InverseTransformPoint(player.position);
                    if (relativePoint.y < 0.0)
                        dir = 1;
                    else if (relativePoint.y > 0.0)
                        dir = -1;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            if(moveDoor || opened)
                closeDoor = true;
        }
    }
}
