﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGrab : MonoBehaviour
{

    public Transform objPlayer;

    private bool grabbed = false;

    // Update is called once per frame
    void Update()
    {
        if (grabbed)
        {
            transform.position = objPlayer.position;
            transform.rotation = objPlayer.rotation;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!ItemManager.instance.item && Input.GetButtonDown("Fire1"))
            {
                grabbed = true;
                ItemManager.instance.item = gameObject;
                AkSoundEngine.PostEvent("El_Cuadro", gameObject);
            }
        }
    }
}