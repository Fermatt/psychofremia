﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransportMonster : MonoBehaviour {

    [SerializeField] private EnemyBehaviour e;
    [SerializeField] private Transform pos;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            e.changePos(pos);
        }
    }
}
