﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomRotation : MonoBehaviour {

    [SerializeField] private GameObject[] rooms;
    [SerializeField] private EnemyBehaviour monster;
    [SerializeField] private Transform posMonster;

	// Use this for initialization
	void Start () {
        RoomManager.instance.changeRoom = false;

    }
	
	// Update is called once per frame
	void Update () {
		if(RoomManager.instance.changeRoom && RoomManager.instance.canRotate)
        {
            if(RoomManager.instance.doorsClosed)
            {
                Vector3 firstPos = rooms[0].transform.position;
                Quaternion firstRot = rooms[0].transform.rotation;
                for (int i = 0; i < rooms.Length; i++)
                {
                    if(i < rooms.Length-1)
                    {
                        rooms[i].transform.position = rooms[i + 1].transform.position;
                        rooms[i].transform.rotation = rooms[i + 1].transform.rotation;
                    }
                    else
                    {
                        rooms[i].transform.position = firstPos;
                        rooms[i].transform.rotation = firstRot;
                    }
                }
                RoomManager.instance.changeRoom = false;
                RoomManager.instance.canRotate = false;
                monster.changePos(posMonster);
            }
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            RoomManager.instance.changeRoom = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            RoomManager.instance.changeRoom = true;
        }
    }
}
