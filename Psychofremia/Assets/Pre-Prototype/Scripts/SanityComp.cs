﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanityComp : MonoBehaviour {

    [SerializeField] private int sanity;
    private bool isPlaying = false;

    public void downSanity(int s)
    {
        sanity -= s;
    }

    private void Update()
    {
        if(sanity < 50 && !isPlaying)
        {
            AkSoundEngine.PostEvent("Player_life", gameObject);
            isPlaying = true;
        }
        else if (sanity >= 50)
        {
            isPlaying = false;
        }
    }
}
