﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unactive : MonoBehaviour {

    [SerializeField] private EnemyBehaviour e;
    [SerializeField] private Transform startPoint;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            e.UnactivateMonster(startPoint);
        }
    }
}
